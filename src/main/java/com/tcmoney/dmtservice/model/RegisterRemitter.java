package com.tcmoney.dmtservice.model;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import lombok.Data;
@Data
public class RegisterRemitter {

	
	@NotNull(message = "Remitter Number Cannot Be Empty")
	@Column(length = 10)
	private String remMobile;
	
	@NotNull(message = "Remitter Name Cannot Be Empty")
	private String remName;
	
	private boolean otpVerified;
	
	private Long remitter_limit_id;

}
