package com.tcmoney.dmtservice.model;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

import lombok.Data;
@Data
public class RegisterBeneficiary {

	
	@NotNull(message = "Beneficiary Name Cannot Be Empty")
	private String beneName;
	
	@NotNull(message = "Beneficiary Mobile Cannot Be Empty")
	@Column(length = 10)
	private String beneMobile;
	
	@NotNull(message = "Beneficiary Account Cannot Be Empty")
	private String beneAccount;
	
	@NotNull(message = "Beneficiary IFSC Cannot Be Empty")
	private String beneIfsc;
	
	private Long rem_id;
	
	private boolean isVerified;

}
