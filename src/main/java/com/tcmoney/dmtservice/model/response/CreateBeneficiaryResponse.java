package com.tcmoney.dmtservice.model.response;

import lombok.Data;

@Data
public class CreateBeneficiaryResponse {
	
	private String beneName;
	
	private String beneMobile;
	
	private String beneAccount;
	
	private String beneIfsc;
	
	private Long rem_id;
	
	private boolean isVerified;


}
