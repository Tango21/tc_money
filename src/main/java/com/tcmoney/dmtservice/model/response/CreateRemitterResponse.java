package com.tcmoney.dmtservice.model.response;

import lombok.Data;

@Data
public class CreateRemitterResponse {

	private String remMobile;
	
	private String remName;
	
	private boolean otpVerified;
	
	private Long remitter_limit_id;


}
