package com.tcmoney.dmtservice.service;

import com.tcmoney.dmtservice.dto.BeneficiaryDto;
import com.tcmoney.dmtservice.dto.RemitterDto;

public interface DmtService {
	public RemitterDto createRemitter(RemitterDto remitter);
	public BeneficiaryDto createBeneficiary(BeneficiaryDto beneficiary);
}
