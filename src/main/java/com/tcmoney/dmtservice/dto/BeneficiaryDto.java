package com.tcmoney.dmtservice.dto;



import java.io.Serializable;

import lombok.Data;

@Data
public class BeneficiaryDto implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4914228217659912748L;
	
	private String beneName;
	
	private String beneMobile;
	
	private String beneAccount;
	
	private String beneIfsc;
	
	private Long rem_id;

	private boolean isVerified;

}
