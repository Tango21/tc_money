package com.tcmoney.dmtservice.dto;

import java.io.Serializable;



import lombok.Data;
@Data
public class RemitterDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 742600437816079220L;
	
	private String remMobile;
	
	private String remName;
	
	private boolean otpVerified;

	private Long remitter_limit_id;

}
