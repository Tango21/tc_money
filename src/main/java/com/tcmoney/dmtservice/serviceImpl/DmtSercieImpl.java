package com.tcmoney.dmtservice.serviceImpl;

import java.util.Calendar;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tcmoney.dmtservice.data.entity.BeneficiaryEntity;
import com.tcmoney.dmtservice.data.entity.RemitterEntity;
import com.tcmoney.dmtservice.data.entity.RemitterLimitEntity;
import com.tcmoney.dmtservice.data.repository.BeneficiaryRepository;
import com.tcmoney.dmtservice.data.repository.RemitterRepository;
import com.tcmoney.dmtservice.dto.BeneficiaryDto;
import com.tcmoney.dmtservice.dto.RemitterDto;
import com.tcmoney.dmtservice.service.DmtService;

import lombok.AllArgsConstructor;
@Service
@AllArgsConstructor
public class DmtSercieImpl implements DmtService{
	private RemitterRepository remitterRepository;
	private BeneficiaryRepository beneficiaryRepository;
	
	@Override
	@Transactional
	public RemitterDto createRemitter(RemitterDto remitter) {
		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		RemitterEntity remitterEntity = mapper.map(remitter, RemitterEntity.class);
		RemitterLimitEntity remitterLimitEntity = new RemitterLimitEntity();
		remitterLimitEntity.setCashLimit(200000);
		remitterLimitEntity.setMonth(Calendar.getInstance().get(Calendar.MONTH) + 1);
		remitterLimitEntity.setYear(Calendar.getInstance().get(Calendar.YEAR));
		//remitterLimitRepository.save(remitterLimitEntity);
		 remitterEntity.setRemitterLimitEntity(remitterLimitEntity);
				remitterRepository.save(remitterEntity);
		RemitterDto remitterDto = mapper.map(remitterEntity, RemitterDto.class);
		return remitterDto;
	}
	
	@Override
	@Transactional
	public BeneficiaryDto createBeneficiary(BeneficiaryDto beneficiary) {
		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		BeneficiaryEntity beneficiaryEntity = mapper.map(beneficiary, BeneficiaryEntity.class);
		beneficiaryRepository.save(beneficiaryEntity);
		BeneficiaryDto beneficiaryDto = mapper.map(beneficiaryEntity, BeneficiaryDto.class);
		return beneficiaryDto;
	}
}
