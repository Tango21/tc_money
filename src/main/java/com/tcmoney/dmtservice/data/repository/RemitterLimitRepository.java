package com.tcmoney.dmtservice.data.repository;

import org.springframework.data.repository.CrudRepository;

import com.tcmoney.dmtservice.data.entity.RemitterLimitEntity;

public interface RemitterLimitRepository extends CrudRepository<RemitterLimitEntity,Long>{

}
