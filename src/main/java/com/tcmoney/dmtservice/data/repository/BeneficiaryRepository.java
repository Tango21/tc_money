package com.tcmoney.dmtservice.data.repository;

import org.springframework.data.repository.CrudRepository;

import com.tcmoney.dmtservice.data.entity.BeneficiaryEntity;

public interface BeneficiaryRepository extends CrudRepository<BeneficiaryEntity,Long>{

}
