package com.tcmoney.dmtservice.data.repository;

import org.springframework.data.repository.CrudRepository;

import com.tcmoney.dmtservice.data.entity.RemitterEntity;

public interface RemitterRepository extends CrudRepository<RemitterEntity,Long>{

}
