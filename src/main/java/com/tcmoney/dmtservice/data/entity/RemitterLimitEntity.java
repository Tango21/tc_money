package com.tcmoney.dmtservice.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
@Data
@Entity
@Table(name="remitter_limit")
public class RemitterLimitEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4469010020755317592L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 6)
	private double cashLimit;
	
	@Column(nullable = false, length = 2)
	private int month;
	
	@Column(nullable = false, length = 4)
	private int year;

}
