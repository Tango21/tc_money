package com.tcmoney.dmtservice.data.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
@Data
@Entity
@Table(name="remitter")
public class RemitterEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5980550690063632022L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 10)
	private String remMobile;
	
	@Column(nullable = false, length = 50)
	private String remName;
	
	@Column(nullable = false, length = 1, columnDefinition = "boolean default false")
	private boolean otpVerified = false;
	
	@OneToMany(mappedBy = "remitterEntity", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<BeneficiaryEntity> beneficiaryEntity;
	
	@OneToOne
	@JoinColumn(name="remitter_limit_id",referencedColumnName="id",insertable=false,updatable=false)
	private RemitterLimitEntity remitterLimitEntity;
}
