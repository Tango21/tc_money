package com.tcmoney.dmtservice.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.tcmoney.dmtservice.model.RegisterRemitter;

import lombok.Data;
@Data
@Entity
@Table(name="beneficiary")
public class BeneficiaryEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1755058219834569681L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, length = 100)
	private String beneName;
	
	@Column(nullable = false, length = 10)
	private String beneMobile;
	
	@Column(nullable = false, length = 30)
	private String beneAccount;
	
	@Column(nullable = false, length = 20)
	private String beneIfsc;
	
	@Column(nullable = false, length = 1, columnDefinition = "boolean default false")
	private boolean isVerified = false;
	@ManyToOne
	@JoinColumn(name = "rem_id",referencedColumnName="id",insertable=false,updatable=false)
	private RemitterEntity remitterEntity;
	
}
