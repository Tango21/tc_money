package com.tcmoney.dmtservice.controllers;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tcmoney.dmtservice.dto.BeneficiaryDto;
import com.tcmoney.dmtservice.dto.RemitterDto;
import com.tcmoney.dmtservice.model.RegisterBeneficiary;
import com.tcmoney.dmtservice.model.RegisterRemitter;
import com.tcmoney.dmtservice.model.response.CreateBeneficiaryResponse;
import com.tcmoney.dmtservice.model.response.CreateRemitterResponse;
import com.tcmoney.dmtservice.service.DmtService;

@RestController
@RequestMapping("/dmt")
public class DmtController {
	@Autowired
	DmtService dmtService;

	@GetMapping("/status/check")
	public String status() {

		return "Application Running perfectly";
	}

	@PostMapping("/create/remitter")
	public ResponseEntity<CreateRemitterResponse> createRemitter(@Valid @RequestBody RegisterRemitter remitterDetails) {

		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		RemitterDto remitter = mapper.map(remitterDetails, RemitterDto.class);
		RemitterDto remitterDto = dmtService.createRemitter(remitter);

		CreateRemitterResponse createRemitterResponse = mapper.map(remitterDto, CreateRemitterResponse.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(createRemitterResponse);

	}

	@PostMapping("/createBeneficiary")
	public ResponseEntity<CreateBeneficiaryResponse> createBeneficiary(
			@Valid @RequestBody RegisterBeneficiary beneficiaryDetails) {
		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		BeneficiaryDto beneficiary = mapper.map(beneficiaryDetails, BeneficiaryDto.class);
		BeneficiaryDto beneficiaryDto = dmtService.createBeneficiary(beneficiary);

		CreateBeneficiaryResponse createBeneficiaryResponse = mapper.map(beneficiaryDto,
				CreateBeneficiaryResponse.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(createBeneficiaryResponse);
	}
}
