package com.tcmoney.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TcMoneyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TcMoneyApplication.class, args);
	}

}
